#include<set>
#include<iostream>
using namespace std;

set<int> get_digits(int n);

int main() {
  int x = 1;
  while(x) {
    if(get_digits(2*x) == get_digits(3*x) &&
       get_digits(2*x) == get_digits(4*x) &&
       get_digits(2*x) == get_digits(5*x) &&
       get_digits(2*x) == get_digits(6*x) ) {
      cout << x << endl;
      break;
    }
    x++;
  }
  return 0;
}

set<int> get_digits(int n) {
  set<int> res;
  while (n) {
    res.insert(n % 10);
    n /= 10;
  }
  return res;
}
