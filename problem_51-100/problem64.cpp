#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

class FRAC {
private:
  int sq;
  int nom;
  int den;
public:
  FRAC(int s, int n=0, int d=1);
  int get_a() const;
  FRAC expand() const;
  friend bool operator==(const FRAC f1, const FRAC f2);
};

bool is_rational(int n);
int get_continued_fraction_count(int n);

int main() {
  int odd_cnt = 0;
  for (int i = 2; i <= 10000; i++) {
    if (is_rational(i))
      continue;
    int cnt = get_continued_fraction_count(i);
    if (cnt % 2 == 1)
      odd_cnt += 1;
  }
  cout << odd_cnt << endl;
  return 0;
}

bool operator==(const FRAC f1, const FRAC f2) {
  if ( (f1.sq == f2.sq) &&
       (f1.nom == f2.nom) &&
       (f1.den == f2.den) )
    return true;
  return false;
}

FRAC::FRAC(int s, int n, int d) {
  sq = s;
  nom = n;
  den = d;
}

int FRAC::get_a() const {
  int a = ((int) sqrt(sq) + nom)/den;
  return a;
}

FRAC FRAC::expand() const {
  int a = this->get_a();
  int n = a * den -nom;
  int d = (sq - n*n)/den;
  FRAC a1 = FRAC(sq, n, d);
  return a1;
}

int get_continued_fraction_count(int n) {
  FRAC f = FRAC(n);
 
  vector<FRAC> fv;
  vector<int> av;
  int a = f.get_a();
  av.push_back(a);
  FRAC nf = f.expand();
  fv.push_back(nf);
  
  int begin;
  while(true) {
    bool flag = false;
    FRAC new_frac = nf.expand();
    int a = nf.get_a();
    for (int i = 0; i < fv.size(); i++) {
      if (new_frac == fv[i]) {
	begin = i+1;
	flag = true;
	break;
      }
    }

    fv.push_back(new_frac);
    av.push_back(a);
    if (flag)
      break;
    nf = new_frac;
  }
  int res = av.size() - begin;
  return res;
}

bool is_rational(int n) {
  double x = sqrt(n);
  if (floor(x) * floor(x) == n)
    return true;
  return false;
}
