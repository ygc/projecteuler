#include<iostream>
using namespace std;

int hcf(int n, int d);

int main() {
  double left=(double)1/8;
  double mid = (double)3/7;
  int numerator;

  for (int d = 3; d <= 1e6; d++) {
    int num = d * mid;
    while (hcf(num, d) != 1) {
      num -= 1;
    }
    double r = (double) num/d;
    if ( r < mid & r > left ) {
      left = r;
      numerator = num;
    }
  }
  cout << "Answers to PE 71: " << numerator << endl;
}

int hcf(int n, int d) {
  int m = d % n;
  if (m == 0)
    return n;
  if (m == 1)
    return 1;
  while(m != 0) {
    d = n;
    n = m;
    m = d % n;
  }
  return n;
}
