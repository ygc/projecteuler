#include<iostream>
#include<vector>
using namespace std;

vector<int> num2vec(int);
vector<int> rev(vector<int>);
vector<int> rev_add(vector<int>);
bool is_palindromic(vector<int>);
bool is_lychrel(int);

int main() {
  int cnt=0;
  for (int i=0; i < 10000; i++) {
    if (is_lychrel(i)) {
      cnt++;
    }
  }
  cout << cnt << endl;
  return 0;
}


vector<int> num2vec(int n) {
  vector<int> res;
  while(n) {
    res.push_back(n % 10);
    n = n / 10;
  }
  res = rev(res);
  return res;
}

vector<int> rev(vector<int> x) {
  int n=x.size();
  vector<int> res(n);
  for (int i=0; i < n; i++) {
    res[i] = x[n-i-1];
  }
  return res;
}

bool is_palindromic(vector<int> v) {
  if ( v == rev(v) )
    return 1;
  return 0;
 }

vector<int> rev_add(vector<int> x) {
  int n = x.size();
  vector<int> y=rev(x);
  vector<int> res(n);
 
  for (int i=0; i < n; i++) 
    res[i] = x[i] + y[i];
  for (int i=n-1; i >= 0; i--) {
    if (res[i] >= 10) {
      if (i == 0) {
	res[i] -= 10; 
	res.insert(res.begin(), 1);
      } else {
	res[i-1] += 1;
	res[i] -= 10;
      }
    }
  }
  return res;
}

bool is_lychrel(int x) {
  vector<int> y = num2vec(x);
  for (int i=0; i < 50; i++){
    // 50 iterations for only support number below 10000;
    y = rev_add(y);
    if (is_palindromic(y))
      return 0;
  }
  return 1;
}

