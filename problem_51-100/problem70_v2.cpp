#include<iostream>
#include<cmath>
#include<vector>
#include<limits>

using namespace std;

const int N=1e7;

bool isPerm(int x, int y);
int totient(int n);

int main() {
  double minRatio = std::numeric_limits<double>::max();
  int res = 0;
  for (int n = 2; n < N; n++) {
    int p = totient(n);
    if (isPerm(n, p)) {
      double ratio = (double)n/p;
      if (ratio < minRatio) {
	minRatio = ratio;
	res = n;
      }
    }
  }
  cout << "Answers of PE 70: " << res << endl;
  return 0;
}


int totient(int n) {
  static int phis[N] = {0, 1, 1};
  if (phis[n] != 0) {
    return(phis[n]);
  }
  for (int p=2; p <= sqrt(n); p++) {
    if ( n % p == 0 ) {
      int pk = p;
      while ( (n % (pk * p) == 0) ) {
	pk *= p;
      }
      if ( pk == n ) {
	// phis[n] = p^k
	phis[n] = n - n/p;
	return phis[n];
      }
      if ( phis[pk] == 0 ) {
	phis[pk] = pk - pk/p;
      }
      phis[n] = totient(pk) * totient(n/pk);
      return phis[n];
    }
  }
  phis[n] = n-1;
  return phis[n];
}

bool isPerm(int x, int y) {
  vector<int> xv, yv;
  while ( x > 10 ) {
    xv.push_back(x % 10);
    x /= 10;
  }
  xv.push_back(x);

  while ( y > 10 ) {
    yv.push_back(y % 10);
    y /= 10;
  }
  yv.push_back(y);

  if (xv.size() != yv.size())
    return false;

  sort(xv.begin(), xv.end());
  sort(yv.begin(), yv.end());

  for (int i=0; i<xv.size(); i++) {
    if (xv[i] != yv[i])
      return false;
  }
  return true;
}
