#include<iostream>
#include<cmath>
#include<vector>

using namespace std;

const int N=1e7;

bool isPrime(int n);
bool isPerm(int x, int y);

int main() {
  vector<int> primes;
  primes.push_back(2);
  for (int i=2000; i<5000; i+=1) {
    if(isPrime(i)) {
      primes.push_back(i);
    }
  }
  int res=0;
  int n = 1;
  double minRatio = 2.0;
  for ( int i=primes.size()-1; i > 0; i--) {
    for (int j=i-1; j>=0; j--) {
      n = primes[i] * primes[j];
      if (n > N)
	continue;
      int phi = (primes[i]-1)*(primes[j]-1);
      if (isPerm(n, phi)) {
	double ratio = double(n)/phi;
	if (ratio < minRatio) {
	  minRatio = ratio;
	  res=n;
	}
      }
    }

  }
  
  cout << "Answers of PE 70: " << res << endl;
  return 0;
}

bool isPerm(int x, int y) {
  vector<int> xv, yv;
  while ( x > 10 ) {
    xv.push_back(x % 10);
    x /= 10;
  }
  xv.push_back(x);

  while ( y > 10 ) {
    yv.push_back(y % 10);
    y /= 10;
  }
  yv.push_back(y);
 
  if (xv.size() != yv.size())
    return false;
  
  sort(xv.begin(), xv.end());
  sort(yv.begin(), yv.end());
  
  for (int i=0; i<xv.size(); i++) {
    if (xv[i] != yv[i])
      return false;
  }
  return true;
}

bool isPrime(int n) {
  if (n == 2)
    return true;
  if (n % 2 == 0)
    return false;
  for (int i=2; i <= sqrt(n); i++) {
    if ( n % i == 0 )
      return false;
  }
  return true;
}
