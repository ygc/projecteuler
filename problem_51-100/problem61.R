getPolygonalNumber <- function(n, type, ndigit=4) {
    pType <- c("triangle", "square", "pentagonal",
               "hexagonal", "heptagonal", "octagonal")
    if (type < 3 || type > 8) {
        stop("type should be integer in [3,8]")
    }
    p <- switch(pType[type-2],
                "triangle" = n*(n+1)/2,
                "square" = n^2,
                "pentagonal" = n*(3*n-1)/2,
                "hexagonal" = n*(2*n-1),
                "heptagonal" = n*(5*n-3)/2,
                "octagonal" = n*(3*n-2)
                )

    nd <- 10^(ndigit-1)
    idx <- p/nd > 1 & p/nd <10
    if (length(idx) == 0) {
        return(NA)
    }

    p <- p[idx]
    p.df <- t( sapply(p, function(i)
                      c(floor(i/100), i%%100)
                      )
              )

    p.df <- p.df[ p.df[,2] != 0, ]
    p.df <- p.df[ p.df[,2] > 10, ]
    return(p.df)
}

findConnected <- function(pl, dfx) {
    p <- pl$p
    type <- pl$type
    nc <- ncol(p)
    nr <- nrow(p)

    xx <- lapply(1:nr, function(n) {
        idx <- which(p[n,nc] == dfx$start)
        list(p=t(sapply(idx, function(i) c(p[n,], dfx$end[i]))),
             type=t(sapply(idx, function(i) c(type[n,], dfx$type[i]))))
    })

    pp <- lapply(xx, function(i) i$p)
    type <- lapply(xx, function(i) i$type)

    i <- sapply(pp, ncol) != 0 | sapply(type, ncol) != 0
    pp <- pp[i]
    type <- type[i]

    pp <- do.call(rbind, pp)
    type <- do.call(rbind, type)

    idx <- apply(type, 1, function(j)
                 length(unique(j)) == length(j)
                 )
    pp <- pp[idx,]
    type <- type[idx,]

    result <- list(p=pp, type=type)
    return(result)
}

findCycle <- function(pp, size=6) {
    n <- length(pp)

    type <- matrix(rep(n+2, nrow(pp[[n]])), ncol=1)
    type <- as.data.frame(type)
    ps <- list(p=pp[[n]], type=type)

    dfx <- do.call(rbind, pp[-n])
    dfx <- as.data.frame(dfx)
    colnames(dfx) <- c("start", "end")
    dfx$type <- rep(3:(n+1), times=sapply(pp[-n], nrow))

    for (i in 1:(size-1)) {
        ps <- findConnected(ps, dfx)
    }

    idx <- ps$p[,1] == ps$p[,ncol(ps$p)]
    result <- list(p=ps$p[idx,], type=ps$type[idx,])

    return(result)
}

pp <- lapply(3:8, getPolygonalNumber, n=10:150, ndigit=4)
result <- findCycle(pp, size=6)

print(result)
s <- 101 * sum(unique(result$p))
cat("Answer of PE 61:",  s, "\n")
