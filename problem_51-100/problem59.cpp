#include<iostream>
#include<fstream>
#include<vector>
using namespace std;


int main() {
  ifstream in;
  in.open("cipher1.txt");
  vector<int> encryptedNum;
  int encryptedChar;

  while(in){
    in >> encryptedChar;
    encryptedNum.push_back(encryptedChar);
    in.get(); // drop character ,
  }
  in.close();

  int n=3;
  char key[n];
  char ch;
  int minCount;

  for (int i=0; i < n; i++) {
    int cnt = 0;
    for (int j=97; j <=122; j++) {
      for (int k=i; k < encryptedNum.size(); k += 3) {
	char x = j^encryptedNum[k];

	if ( (x >= 65 && x <= 122) || x == 32) {
	  // if x were character [a-z] [A-Z] or space
	} else {
	  cnt++;
	}

      }
      if (j == 97) {
	minCount = cnt;
	ch = j;
      } else if (cnt < minCount) {
	minCount = cnt;
	ch = j;
      }
      cnt = 0;
    }
    key[i] = ch;
  }

  int sum = 0;
  int idx = 0;

  cout << endl << "The original text is: "
       << endl << endl;
  for (int i=0; i<encryptedNum.size()-1; i++) {
    idx %= 3;
    char val = key[idx]^encryptedNum[i];
    idx++;
    cout << val;
    sum += val;
  }
  cout << endl << endl;

  cout << "The sum of the ASCII values in the original text is "
       << sum << endl;
}
