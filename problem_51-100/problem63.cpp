#include<iostream>
#include<cmath>

using namespace std;

int main() {
  int res = 0;
  for (int i=1; i <= 9; i++) {
    res += 1/(1-log10(i));
  }
  cout << "Answer: " << res << endl;
}
