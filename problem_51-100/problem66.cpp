#include <iostream>
#include <cmath>
#include <vector>
#include "stdio.h"
#include "gmp.h"
#include "gmpxx.h"

using namespace std;

class FRAC {
private:
  int sq;
  int nom;
  int den;
public:
  FRAC(int s, int n=0, int d=1);
  int get_a() const;
  FRAC expand() const;
  friend bool operator==(const FRAC f1, const FRAC f2);
};

bool is_rational(int n);
vector<int> get_continued_fraction(int n);
mpz_class est_num(vector<int> av);

int main() {
  int D_max_X;
  mpz_class max_X=0;
  for (int D=2; D <= 1000; D++) {
    if (sqrt(D) * sqrt(D) != D) {
      vector<int> av = get_continued_fraction(D);
      mpz_class x = est_num(av);
      if (max_X < x) {
	max_X = x;
	D_max_X = D;
	cout << "(D, X)" << "\t" << D_max_X << "\t";
	gmp_printf("%Zd\n", max_X.get_mpz_t());
      }
    }
  }
  return 0;
}

bool operator==(const FRAC f1, const FRAC f2) {
  if ( (f1.sq == f2.sq) &&
       (f1.nom == f2.nom) &&
       (f1.den == f2.den) )
    return true;
  return false;
}

FRAC::FRAC(int s, int n, int d) {
  sq = s;
  nom = n;
  den = d;
}

int FRAC::get_a() const {
  int a = ((int) sqrt(sq) + nom)/den;
  return a;
}

FRAC FRAC::expand() const {
  int a = this->get_a();
  int n = a * den -nom;
  int d = (sq - n*n)/den;
  FRAC a1 = FRAC(sq, n, d);
  return a1;
}

vector<int> get_continued_fraction(int n) {
  FRAC f = FRAC(n);

  vector<FRAC> fv;
  vector<int> av;
  int a = f.get_a();
  av.push_back(a);
  FRAC nf = f.expand();
  fv.push_back(nf);

  int begin;
  while(true) {
    bool flag = false;
    FRAC new_frac = nf.expand();
    int a = nf.get_a();
    for (int i = 0; i < fv.size(); i++) {
      if (new_frac == fv[i]) {
	begin = i+1;
	flag = true;
	break;
      }
    }

    fv.push_back(new_frac);
    av.push_back(a);
    if (flag)
      break;
    nf = new_frac;
  }
  return av;
}

bool is_rational(int n) {
  double x = sqrt(n);
  if (floor(x) * floor(x) == n)
    return true;
  return false;
}

mpz_class est_num(vector<int> av) {
  if (av.size() % 2 == 1) {
    av.pop_back();
  } else {
    int n = av.size();
    for (int i=1; i < n-1; i++) {
      av.push_back(av[i]);
    }
  }
  int idx = av.size() -1;
  mpz_class num = 1;
  mpz_class den = av[idx];
  int a = av[idx-1];

  if (av.size() >= 3) {
    for (int i=idx-2; i >= 0; i--) {
      mpz_class newden = a*den+num;
      num = den;
      den = newden;
      a = av[i];
    }
  }

  num = a*den+num;
  return(num);
}

