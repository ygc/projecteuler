#include<iostream>
#include<vector>
using namespace std;
 
double fact(int n);
 
int main() {
  int cnt=0;
  for (int n=1; n <= 100; n++) {
    for (int r=1; r <= n; r++) {
      double comb = fact(n)/(fact(r) * fact(n-r));
      if (comb > 1000000)
	cnt++;
    }
  }
  cout << cnt << endl;
  return 0;
}
 
 
double fact(int n) {
  static vector<double> mem;
  if (n == 0) return 1;
  if (mem.size() >= n) return mem[n-1];
  double res = n*fact(n-1);
  mem.push_back(res);
  return res;
}
