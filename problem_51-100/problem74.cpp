#include<iostream>
#include<map>

using namespace std;
const int N=1e6;

bool chain_of_length_60(int x);
int sum_digit_factorial(int x);
int fac(int i);
int factorial(int n);

int main() {
  int res=0;
  for (int i=70; i<N; i++) 
    if (chain_of_length_60(i))
      res++;
  cout << "Answer of Problem 74 is: " << res << endl;
  return 0;
}

bool chain_of_length_60(int x) {
  static map<int, int> mem;
  int sz = 61;
  int val, vals[sz];
  int length=1;
  bool flag = false;
  vals[0] = x;
  int y = x;
  for (int i=1; i <= sz; i++) {
    if (mem.find(y) == mem.end() ) {
      val = sum_digit_factorial(y);
      mem[y] = val;
    } else {
      val = mem[y];
    }
    y = val;
    for (int j=0; j<i; j++) {
      if (val == vals[j]) {
	flag = true;
	break;
      }
    }
    if (flag)
      break;
    vals[i] = val;
    length++;
  }
  return length == 60;
}

int sum_digit_factorial(int x) {
  int res = 0;
  while (x) {
    res += fac(x % 10);
    x = x / 10;
  }
  return res;
}

int fac(int i) {
  static int fac_mem[10];
  if (i < 0 || i > 9) 
    return NULL;
  if (!fac_mem[i]) {
    fac_mem[i] = factorial(i);
  }
  return fac_mem[i];
}

int factorial(int n) {
  if (n <= 1)
    return 1;
  else 
    n = n * factorial(n-1);
  return n;
}
