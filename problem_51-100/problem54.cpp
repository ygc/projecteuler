#include<iostream>
#include<fstream>
#include<vector>
using namespace std;
 
vector<int> get_pairs(vector<int>);
 
class CARD {
private:
  const static int CARD_SIZE = 5;
  char card[CARD_SIZE][2];
  int get_card_rank();
  int get_high_card();
  bool is_royal_flush();
  bool is_straight_flush();
  bool is_four_of_a_kind();
  bool is_full_house();
  bool is_flush();
  bool is_straight();
  bool is_three_of_a_kind();
  bool is_two_pairs();
  bool is_one_pair();
  bool is_same_suit();
  vector<int> card_to_vector();
public:
  void show();
  friend int card_compare(CARD &x, CARD &y);
  friend ifstream &operator>>(ifstream &stream, CARD &obj);
};
 
int main() {
  ifstream in;
  in.open("poker.txt");
  if (!in.is_open()) {
    cout << "File is not open." << endl;
    return 1;
  }
 
  vector<int> res;
 
  while(!in.eof()) {
    CARD p1;
    CARD p2;
    in >> p1;
    in >> p2;
 
    int ans = card_compare(p1,p2);
    res.push_back(ans);
  }
  in.close();
 
  int cnt = 0;
 
  for (int i=0; i < res.size(); i++) {
    cnt += res[i];
  }
 
  cout << cnt << endl;
  return 0;
}
 
void CARD::show() {
  for (int i=0; i<CARD_SIZE; i++) {
    cout << (this->card)[i][0] << "\t" 
	 << (this->card)[i][1] << endl;
  }
}
 
int card_compare(CARD &x, CARD &y) {
  int high = 100;
  int xr = x.get_card_rank();
  int yr = y.get_card_rank();
 
  vector<int> xv = x.card_to_vector();
  vector<int> yv = y.card_to_vector();
 
  if (xr > yr) {
    return 1;
  } else if ( xr < yr ) {
    return 0;
  } else {
    if (x.is_full_house()) {
      vector<int> xp = get_pairs(xv);
      vector<int> xpp = get_pairs(xp);
      vector<int> yp = get_pairs(yv);
      vector<int> ypp = get_pairs(yp); 
      // xpp and ypp is the three of a kind.
      if (xpp[0] > ypp[0]) {
	return 1;
      } else if (xpp[0] < ypp[0]) {
	return 0;
      } else {
	if (xp[xp.size()-1] > yp[yp.size()-1]) { 
	  return 1;
	} else {
	  return 0;
	}
      }
    } else if (x.is_two_pairs()) {
      vector<int> xp = get_pairs(xv);
      vector<int> yp = get_pairs(yv);
      if (xp[1] > yp [1]) {
	return 1;
      } else if (xp[1] < yp[1]) {
	return 0;
      } else {
	if (xp[0] > yp[0]) {
	  return 1;
	} else if (xp[0] < yp[0]) {
	  return 0;
	} else {
	  int x_high, y_high;
	  for (int i=0; i < x.CARD_SIZE; i++) {
	    if( xv[i] != xp[0] && xv[i] != xp[1] ) {
	      x_high = xv[i];
	    } 
	    if( yv[i] != yp[0] && yv[i] != yp[1] ) {
	      y_high = yv[i];
	    }
	  }
	  if (x_high > y_high) {
	    return 1;
	  } else {
	    return 0;
	  }
	}
      }
    } else if (x.is_one_pair()) {
      vector<int> xp = get_pairs(xv);
      vector<int> yp = get_pairs(yv);
      if(xp[0] > yp[0]) {
	return 1;
      } else if (xp[0] < yp[0]) {
	return 0;
      } 
    } 
  }
  for (int i=(x.CARD_SIZE-1); i >= 0; i--) {
    if (xv[i] > yv[i]) {
      return 1;
    } else if (xv[i] == yv[i]) {
      continue;
    } else {
      return 0;
    }
  } 
}
 
ifstream &operator>>(ifstream &stream, CARD &obj) {
  for (int i=0; i < obj.CARD_SIZE; i++) {
    for (int j=0; j < 2; j++) {
      stream >> obj.card[i][j];
    }
  }
  return stream;
}
 
 
int CARD::get_card_rank() {
  int high = 100;
  if(this->is_royal_flush()) 
    return (high-1);
  if(this->is_straight_flush())
    return (high-2);
  if(this->is_four_of_a_kind())
    return (high-3);
  if(this->is_full_house()) 
    return (high-4);
  if(this->is_flush())
    return (high-5);
  if(this->is_straight())
    return (high-6);
  if(this->is_three_of_a_kind())
    return (high-7);
  if(this->is_two_pairs()) 
    return (high-8);
  if(this->is_one_pair())
    return (high-9);
 
  return (this->get_high_card());
}
 
bool CARD::is_royal_flush() {
  if (!this->is_same_suit())
    return 0;
  if (!this->is_straight())
    return 0;
  bool flag = 1;
  for (int i=0; i < this->CARD_SIZE; i++) {
    flag = ((this->card)[i][0] == 'T' ||
	    (this->card)[i][0] == 'J' ||
	    (this->card)[i][0] == 'Q' ||
	    (this->card)[i][0] == 'K' ||
	    (this->card)[i][0] == 'A');
    if (!flag) {
      break;
    }
  }
  return flag;
}
 
bool CARD::is_straight_flush() {
  if (!this->is_same_suit())
    return 0;
  return(this->is_straight());
}
 
 
bool CARD::is_four_of_a_kind() {
  vector<int> values = this->card_to_vector();
  if (values[0] != values[1]) {
    for (int i=2; i < this->CARD_SIZE; i++) {
      if (values[1] != values[i])
	return 0;
    }
  } else {
    for (int i=1; i < (this->CARD_SIZE-1); i++)
      if (values[0] != values[i])
	return 0;
  }
  return 1;
}
 
bool CARD::is_full_house() {
  vector<int> values = this->card_to_vector();
  if (values[0] == values[1] &&
      values[0] == values[2] &&
      values[3] == values[4])
    return 1;
  if (values[0] == values[1] &&
      values[2] == values[3] &&
      values[2] == values[4])
    return 1;
  return 0;
}
 
bool CARD::is_flush() {
  if(this->is_same_suit())
    return 1;
  return 0;
}
bool CARD::is_straight() {
  vector<int> values = this->card_to_vector();
  if ( (values[0] + 1) == values[1] &&
       (values[1] + 1) == values[2] &&
       (values[2] + 1) == values[3] &&
       (values[3] + 1) == values[4] )
    return 1;
  return 0;
}
 
bool CARD::is_three_of_a_kind() {
  if(this->is_four_of_a_kind())
    throw 1;
 
  vector<int> values = this->card_to_vector();
   if(values[0] == values[1] &&
     values[0] == values[2])
    return 1;
  if(values[1] == values[2] &&
     values[1] == values[3])
    return 1;
  if(values[2] == values[3] &&
     values[2] == values[4])
    return 1;
  return 0;
}
 
bool CARD::is_two_pairs() {
  if(this->is_three_of_a_kind())
    throw 1;
 
  if(this->is_one_pair()) {
    vector<int> values = this->card_to_vector();
    vector<int> p = get_pairs(values);
    if ( p.size() == 2 )
      return 1;
  }
  return 0;  
}
 
bool CARD::is_one_pair() {
  // this function test if it is has one pair.
  // not has and only has one pair.
  vector<int> values = this->card_to_vector();
  for (int i=0; i < this->CARD_SIZE; i++) {
    for (int j=i+1; j < this->CARD_SIZE; j++) {
      if (values[i] == values[j])
	return 1;
    }
  }
  return 0;
}
 
int CARD::get_high_card() {
  vector<int> values = this->card_to_vector();
  return(values[this->CARD_SIZE-1]);
}
 
bool CARD::is_same_suit() {
  char suit = (this->card)[0][1];
  for (int i=1; i < this->CARD_SIZE; i++) {
    if ( (this->card)[i][1] != suit) {
      return 0;
    }
  } 
  return 1;
}
 
vector<int> CARD::card_to_vector() {
  vector<int> values;
  for (int i=0; i < this->CARD_SIZE; i++) {
    switch((this->card)[i][0]) {
    case '2':
      values.push_back(2);
      break;
    case '3':
      values.push_back(3);
      break;
    case '4':
      values.push_back(4);
      break;
    case '5':
      values.push_back(5);
      break;
    case '6':
      values.push_back(6);
      break;
    case '7':
      values.push_back(7);
      break;
    case '8':
      values.push_back(8);
      break;
    case '9':
      values.push_back(9);
      break;
    case 'T':
      values.push_back(10);
      break;
    case 'J':
      values.push_back(11);
      break;
    case 'Q':
      values.push_back(12);
      break;
    case 'K':
      values.push_back(13);
      break;
    case 'A':
      values.push_back(14);
      break;
    }
  }
  sort(values.begin(), values.end());
  return values;
}
 
vector<int> get_pairs(vector<int> x) {
  vector<int> p;
  for (int i=0; i < x.size(); i++) {
    for (int j=i+1; j < x.size(); j++) {
      if(x[i] == x[j])
	p.push_back(x[i]);
    }
  }
  return p;
}
