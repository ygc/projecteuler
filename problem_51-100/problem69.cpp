#include<iostream>
#include<cmath>
#include<vector>

using namespace std;
const int N=1000000;

bool isPrime(int n);

int main() {
  vector<int> primes;
  for (int i=2; i <= sqrt(N); i++) {
    if (isPrime(i)) {
      primes.push_back(i);
    }
  }
  int n=1;
  for (int i=0; i< primes.size(); i++) {
    if (n * primes[i] > N) {
      cout << "Answers to PE 69: " << n << endl;
      break;
    }
    n *= primes[i];
  }
  return 0;
}

bool isPrime(int n) {
  if (n == 2)
    return true;
  if (n % 2 == 0)
    return false;
  for (int i=2; i <= sqrt(n); i++) {
    if ( n % i == 0 )
      return false;
  }
  return true;
}

