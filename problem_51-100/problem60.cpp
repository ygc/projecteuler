#include<iostream>
#include<cmath>
#include<vector>
#include<fstream>
using namespace std;

bool is_prime(int n);
int concatNum(int a, int b);

const int N=10000;

int main() {
  ofstream out;
  out.open("p60.txt");
  vector<int> primes;
  primes.push_back(2);
  for (int i = 3; i < N; i+=2) {
    if (is_prime(i))
      primes.push_back(i);
  }

  for(int i=0; i<primes.size(); i++) {
    for (int j=0; j<i; j++) {
      if (is_prime(concatNum(primes[i], primes[j])) &&
	  is_prime(concatNum(primes[j], primes[i])) ) {
	out << primes[i] << "," << primes[j] << endl;
      }
    }
  }
  return 0;
}

bool is_prime(int n) {
  if ( n == 2)
    return true;
  if (n < 2)
    return false;
  if ( ! (n % 2) )
    return false;
  for (int i=3; i<=sqrt(n); i+=2) {
    if (n % i == 0)
      return false;
  }
  return true;
}

int concatNum(int a, int b) {
  int res = a * pow((double) 10, (int) log10(b) +1) + b;
  return res;
}
