#include<iostream>
#include<cmath>

using namespace std;

const int N = 12000;

int hcf(int n, int d);

int main() {
  double min = (double) 1/3;
  double max = (double) 1/2;
  int cnt = 0;

  for (int d=3; d <= N; d++ ) {
    int left = d * min + 1;
    int right = d * max;
    if ( (double) right/d == max)
      right -= 1;
    if ( right < left)
      continue;
    for (int n=left; n <= right; n++) {
      if (hcf(n, d) == 1)
	cnt++;
    }
  }

  cout << "Answers to PE 73: " << cnt << endl;
}

int hcf(int n, int d) {
  int m = d % n;
  if (m == 0)
    return n;
  if (m == 1)
    return 1;
  while(m != 0) {
    d = n;
    n = m;
    m = d % n;
  }
  return n;
}
