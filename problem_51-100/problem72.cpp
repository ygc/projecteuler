#include<iostream>
#include<cmath>

using namespace std;

const int N = 1e6+1;

int totient(int n);

int main() {
  long int cnt=0;
  for (int d = 2; d < N; d++) {
    cnt += totient(d);
  }
  cout << "Answers to PE 72: " << cnt << endl;
}

int totient(int n) {
  static int phis[N] = {0, 1, 1};
  if (phis[n] != 0) {
    return(phis[n]);
  }
  for (int p=2; p <= sqrt(n); p++) {
    if ( n % p == 0 ) {
      int pk = p;
      while ( (n % (pk * p) == 0) ) {
	pk *= p;
      }
      if ( pk == n ) {
	// phis[n] = p^k
	phis[n] = n - n/p;
	return phis[n];
      }
      if ( phis[pk] == 0 ) {
	phis[pk] = pk - pk/p;
      }
      phis[n] = totient(pk) * totient(n/pk);
      return phis[n];
    }
  }
  phis[n] = n-1;
  return phis[n];
}


