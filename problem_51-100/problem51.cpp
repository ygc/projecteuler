#include<iostream>
#include<cmath>
using namespace std;
 
const int size = 6;
const int family_size = 8;
 
bool has_same_digits(int n);
bool is_same_digit(int n, int i);
bool is_prime(int n);
int* get_digits(int n);
int* get_position(int n, int i);
int get_num(int* d);
int get_digit_count(int n, int i);
 
int main() {
  for (int n=pow(10.0, size-1); n < pow(10.0,size); n++) {
    bool flag = 0;
    if (is_prime(n) && has_same_digits(n)) { 
      for(int i=0; i < 10; i++){
	if (is_same_digit(n,i)) {
	  int* pos;
	  int* d;
	  d = get_digits(n);
	  pos = get_position(n, i);
	  int cnt=0;
	  int len=get_digit_count(n,i);
	  for (int t=0; t<10; t++) {
	    int idx = 0;
	    while(idx < len) {
	      d[pos[idx]] = t;
	      idx++;
	    }
	    int num=get_num(d);
	    if (is_prime(num) && num > pow(10.0, size-1))
	      cnt++;
	  }
	  if (cnt == family_size) {
	    cout << n << endl;
	    flag = 1;
	    break;
	  }
	}
      }
    }
    if (flag) 
      break;
  }
  return 0;
}
 
 
int* get_digits(int n) {
  int* d = new int[size];
  for (int i=0; i < size; i++) {
    d[i] = n % 10;
    n /= 10;
  }
  return d;
}
 
int get_num(int* d) {
  int num = 0;
  for(int i=0; i < size; i++) {
    num += d[i] * (int) pow(10.0, i);
  }
  return num;
}
 
int get_digit_count(int n, int i) {
  int* d = get_digits(n);
  int cnt = 0;
  for (int j=0; j < size; j++) {
    if (d[j] == i)
      cnt++;
  }
  return cnt;
}
 
int* get_position(int n, int i) {
  int* d = get_digits(n);
  int cnt = get_digit_count(n, i);
  int* p = new int[cnt];
  int p_idx = 0;
  for (int j=0; j < size; j++) {
    if (d[j] == i){
      p[p_idx++] = j; 
    }
  }
  return p;
}
 
bool is_same_digit(int n, int i) {
  int* d = get_digits(n);
  int cnt=0;
  for (int j=0; j < size; j++) {
    if (d[j] == i)
      cnt++;
  }
  if (cnt >= 2)
    return 1;
  return 0;
}
 
bool has_same_digits(int n) {
  int* d = get_digits(n);
  for (int i=0; i < size; i++) {
    for (int j=0; j < i; j++) {
      if (d[i] == d[j])
	return 1;
    }
  }
  return 0;
}
 
bool is_prime(int n) {
  for (int i=2; i < sqrt(n); i++) {
    if ( n % i == 0 ) {
      return 0;
      break;
    }
  }
  return 1;
}
