rmDup <- function(vec) {
    idx <- sapply(vec, function(n) {
        nv <- unlist(strsplit(as.character(n), split=""))
        return(length(unique(nv)) == length(nv))
    }
                  )
   return(vec[idx])
}

n <- 102:999
prime <- c(13,11,7,5,3,2)
d <- n[ n %% 17 ==0]
d <- rmDup(d)

retain <- c()
for (i in 1:length(prime)) {
    for (j in  d) {
        lastdigits <- j %% 10^i
        first2digit <- (j-lastdigits)/10^i
        for (n in 0:9) {
            m <- n*100+first2digit
            if( m %% prime[i] ==0 ) {
                retain <- c(retain,m*10^i+lastdigits)
            }
        }
    }
    d <- rmDup(retain)
    if (i != length(prime))
        retain <- c()
}

s <- 0
for (i in d) {
    if(nchar(as.character(i)) == 9) {
        xx <- 0:9
        firstDigit <- xx[!xx %in% unlist(strsplit(as.character(i), split=""))]
        s <- s+ firstDigit*10^9+i
    }
    if(nchar(as.character(i)) == 8) {
        xx <- 1:9
        firstDigit <- xx[!xx %in% unlist(strsplit(as.character(i), split=""))]
        if(length(firstDigit) == 1)
            s <- s+ firstDigit*10^9+i
    }
}

print(s)
