n <- 9999:1000
prime <- n[gmp::isprime(n) != 0]
pl <- lapply(prime,function(i) unlist(strsplit(as.character(i), split="")))

flag <- 0
for (i in seq_along(pl)) {
    x <- pl[[i]]
    maxP <- prime[i]

    pl <- pl[-i]
    prime <- prime[-i]

    idx <- unlist(lapply(pl, function(i) all(x %in% i) & all(i %in% x)))
    idx <- which(idx)
    if (length(idx) >= 2) {
        sel <- prime[idx]
        diff <- maxP-sel
        for (j in 1:length(diff)) {
            m <- which(diff == 2* diff[j])
            if (length(m) >= 1) {
                minP <- sel[m[length(m)]]
                midP <- sel[j]
                flag <- 1
                break
            }
        }
    }
    if(flag) {
        break
    }
}

ans <- paste(c(minP, midP, maxP), collapse="")
print(ans)
