numbers = 1:1000;
primer = numbers(find(isprime(numbers) ==1));

len = zeros(1,length(primer));
for i = 1:length(primer)
	len(i) = digitRecurringLength(primer(i));
end

answer = primer(find(len == max(len)));

fprintf(['the number which contains the longest recurring cycle is : $%f\n'], answer);