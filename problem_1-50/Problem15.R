find.routes.internal <- function(x,y) {
    cnt <- cacheMat[x+1,y+1]
    if (cnt != 0)
        return(cnt)

    if (y >0)
        cnt <- cnt+find.routes.internal(x,y-1)
    if (x >0)
        cnt <- cnt+find.routes.internal(x-1,y)
    if (x ==0 && y ==0)
        cnt <- cnt+1

    cacheMat[x+1,y+1] <<- cnt

    return(cnt)
}

find.routes <- function(x,y) {
    cacheMat <- matrix(0, nrow=x+1, ncol=y+1)
    cnt <- find.routes.internal(x,y)
    return(cnt)
}


