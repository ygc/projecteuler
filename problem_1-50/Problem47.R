getFactor <- function(n) {
    f <- c()
    for ( i in 2:ceiling(sqrt(n/2)))  {
        if (n %%i ==0) {
            n <- n/i
            while(n %% i ==0) {
                n <- n/i
            }
            f <- c(f,i)
            if (gmp::isprime(n) !=0) {
                f <- c(f,n)
            }
        }
    }
    return(unique(f))
}



i <- 4
n <- 10^(i-1)

while(TRUE) {
    flag <- 0
    for (j in 0:(i-1)) {
        f <- getFactor(n+j)
        if(length(f) != i)
            break
        if(any(gmp::isprime(f) == 0))
            break
        if (j==i-1)
            flag <- 1
    }
    if (j == i-1 && flag==1) {
        print(n)
        break
    }
    n <- n+j+1
}

