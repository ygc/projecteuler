function length = digitRecurringLength(n)

rest = 1;
length = 0;
flag = 1;
restold=rest;
while flag
rest = mod(rest * 10, n);

length = length + 1;
if  rest==1 | rest==0 | rest==restold 
  flag = 0;
end
restold = rest;
end

end


